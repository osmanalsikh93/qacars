package swh.com.qacars.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.List;

import swh.com.qacars.R;
import swh.com.qacars.adapters.viewHolders.DefViewHolder;
import swh.com.qacars.adapters.viewHolders.ImageViewHolder;
import swh.com.qacars.common.Functions;
import swh.com.qacars.interfaces.OnClickRowListener;
import swh.com.qacars.models.DefModel;

/**
 * ============================================================================
 * 😎😎😎
 * Copyright (c) 11/Jan/2020
 * İstanbul - Türkiye
 * QACars
 * ============================================================================
 **/

public class ImagesAdapter extends RecyclerView.Adapter<ImageViewHolder> {

    private AppCompatActivity activity;
    private OnClickRowListener clickRowListener;
    private List<String> imagesList;
    private boolean isThumbs;


    public ImagesAdapter(AppCompatActivity activity, List<String> imagesList, boolean isThumbs) {
        this.activity = activity;
        this.clickRowListener = (OnClickRowListener) activity;
        this.imagesList = imagesList;
        this.isThumbs = isThumbs;
    }

    @NonNull
    @Override
    public ImageViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ImageViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.row_image, parent, false));
    }

    @Override
    public int getItemCount() {
        return imagesList.size();
    }

    @Override
    public void onBindViewHolder(@NonNull final ImageViewHolder holder, int position) {

        if (isThumbs) {
            holder.picIV.getLayoutParams().width = getPicWidth();
            holder.picIV.getLayoutParams().height = getPicHeight();

            ((ViewGroup.MarginLayoutParams) holder.picIV.getLayoutParams()).leftMargin = activity.getResources().getDimensionPixelSize(R.dimen.default_spacing_x0_2);
            ((ViewGroup.MarginLayoutParams) holder.picIV.getLayoutParams()).rightMargin = activity.getResources().getDimensionPixelSize(R.dimen.default_spacing_x0_2);

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    clickRowListener.onClickRow(holder.getAdapterPosition());
                }
            });
        }

        if (imagesList.get(holder.getAdapterPosition()) != null && !imagesList.get(holder.getAdapterPosition()).isEmpty())
            Picasso.get().load(imagesList.get(holder.getAdapterPosition()))
                    .resize(getPicWidth(), getPicHeight())
                    .centerCrop()
                    .into(holder.picIV);
    }

    private int getPicWidth() {
        return isThumbs ? Functions.getScreenWidth() / 6 : Functions.getScreenWidth();
    }

    private int getPicHeight() {
        return isThumbs ? Functions.getScreenWidth() / 6 : (int) (Functions.getScreenWidth() / 1.2);
    }
}