package swh.com.qacars.adapters.viewHolders;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import swh.com.qacars.R;

/**
 * ============================================================================
 * 😎😎😎
 * Copyright (c) 12/Jan/2020
 * İstanbul - Türkiye
 * QACars
 * ============================================================================
 **/

public class AdViewHolder extends RecyclerView.ViewHolder {

    public TextView titleTV, priceTV;
    public ImageView starIV, featuredIV, picIV;
    public RecyclerView defRV;

    public AdViewHolder(@NonNull View itemView) {
        super(itemView);

        initializing();
    }

    private void initializing() {
        titleTV = itemView.findViewById(R.id.row_ad_title_text);
        priceTV = itemView.findViewById(R.id.row_ad_price_text);

        starIV = itemView.findViewById(R.id.row_ad_star_image);
        featuredIV = itemView.findViewById(R.id.row_ad_featured_image);
        picIV = itemView.findViewById(R.id.row_ad_pic_image);
        defRV = itemView.findViewById(R.id.row_ad_def_recycler);
    }
}
