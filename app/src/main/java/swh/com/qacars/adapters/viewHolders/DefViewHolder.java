package swh.com.qacars.adapters.viewHolders;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import swh.com.qacars.R;

/**
 * ============================================================================
 * 😎😎😎
 * Copyright (c) 12/Jan/2020
 * İstanbul - Türkiye
 * QACars
 * ============================================================================
 **/

public class DefViewHolder extends RecyclerView.ViewHolder {

    public TextView valueTV;
    public ImageView iconIV;

    public DefViewHolder(@NonNull View itemView) {
        super(itemView);

        initializing();
    }

    private void initializing() {
        valueTV = itemView.findViewById(R.id.row_def_value_text);

        iconIV = itemView.findViewById(R.id.row_def_icon_image);
    }
}
