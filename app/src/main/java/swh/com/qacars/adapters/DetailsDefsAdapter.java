package swh.com.qacars.adapters;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import swh.com.qacars.R;
import swh.com.qacars.adapters.viewHolders.DefViewHolder;
import swh.com.qacars.adapters.viewHolders.DetailsDefViewHolder;
import swh.com.qacars.models.DefModel;

/**
 * ============================================================================
 * 😎😎😎
 * Copyright (c) 11/Jan/2020
 * İstanbul - Türkiye
 * QACars
 * ============================================================================
 **/

public class DetailsDefsAdapter extends RecyclerView.Adapter<DetailsDefViewHolder> {

    private AppCompatActivity activity;
    private List<DefModel> defsList;

    public DetailsDefsAdapter(AppCompatActivity activity, List<DefModel> defsList) {
        this.activity = activity;
        this.defsList = defsList;
    }

    @NonNull
    @Override
    public DetailsDefViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new DetailsDefViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.row_details_def, parent, false));
    }

    @Override
    public int getItemCount() {
        return defsList.size();
    }

    @Override
    public void onBindViewHolder(@NonNull DetailsDefViewHolder holder, int position) {
        DefModel defModel = defsList.get(holder.getAdapterPosition());

        holder.nameTV.setText(defModel.getName());
        holder.valueTV.setText(defModel.getValue());

        holder.itemView.setBackgroundColor(ContextCompat.getColor(activity, position % 2 == 0 ? R.color.white : R.color.grayLight));
    }
}