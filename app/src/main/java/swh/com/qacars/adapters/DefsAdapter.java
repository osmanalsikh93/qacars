package swh.com.qacars.adapters;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import swh.com.qacars.R;
import swh.com.qacars.adapters.viewHolders.DefViewHolder;
import swh.com.qacars.common.Functions;
import swh.com.qacars.models.DefModel;

/**
 * ============================================================================
 * 😎😎😎
 * Copyright (c) 11/Jan/2020
 * İstanbul - Türkiye
 * QACars
 * ============================================================================
 **/

public class DefsAdapter extends RecyclerView.Adapter<DefViewHolder> {

    private AppCompatActivity activity;
    private List<DefModel> defsList;

    DefsAdapter(AppCompatActivity activity, List<DefModel> defsList) {
        this.activity = activity;
        this.defsList = defsList;
    }

    @NonNull
    @Override
    public DefViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new DefViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.row_def, parent, false));
    }

    @Override
    public int getItemCount() {
        return defsList.size();
    }

    @Override
    public void onBindViewHolder(@NonNull DefViewHolder holder, int position) {
        DefModel defModel = defsList.get(holder.getAdapterPosition());

        holder.valueTV.setText(defModel.getValue());
        holder.iconIV.setImageDrawable(ContextCompat.getDrawable(activity,
                activity.getResources().getIdentifier(String.format("ic_%s", defModel.getName().toLowerCase()), "drawable", activity.getPackageName())));
    }
}