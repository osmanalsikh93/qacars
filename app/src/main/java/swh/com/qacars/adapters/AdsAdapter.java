package swh.com.qacars.adapters;

import android.content.Intent;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.core.content.res.ResourcesCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.List;
import java.util.Locale;

import swh.com.qacars.R;
import swh.com.qacars.activities.DetailsActivity;
import swh.com.qacars.adapters.viewHolders.AdViewHolder;
import swh.com.qacars.common.CustomTypefaceSpan;
import swh.com.qacars.common.Functions;
import swh.com.qacars.models.AdModel;

import static swh.com.qacars.common.Constants.*;

/**
 * ============================================================================
 * 😎😎😎
 * Copyright (c) 11/Jan/2020
 * İstanbul - Türkiye
 * QACars
 * ============================================================================
 **/

public class AdsAdapter extends RecyclerView.Adapter<AdViewHolder> {

    private AppCompatActivity activity;
    private List<AdModel> adsList;

    public AdsAdapter(AppCompatActivity activity, List<AdModel> adsList) {
        this.activity = activity;
        this.adsList = adsList;
    }

    @NonNull
    @Override
    public AdViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new AdViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.row_ad, parent, false));
    }

    @Override
    public int getItemCount() {
        return adsList.size();
    }

    @Override
    public void onBindViewHolder(@NonNull AdViewHolder holder, int position) {
        final AdModel adModel = adsList.get(holder.getAdapterPosition());

        holder.titleTV.setText(adModel.getTitle());
        holder.priceTV.setText(Functions.getPriceText(activity, adModel.getPrice(), adModel.getCurrencySymbol()));

        // set pic width and height
        holder.picIV.getLayoutParams().width = getPicWidth();
        holder.picIV.getLayoutParams().height = getPicHeight();

        //holder.starIV.setVisibility(adModel.is);
        holder.featuredIV.setVisibility(adModel.isFeatured() ? View.VISIBLE : View.GONE);

        if (adModel.getImageUrl() != null && !adModel.getImageUrl().isEmpty())
            Picasso.get().load(adModel.getImageUrl())
                    .resize(getPicWidth(), getPicHeight())
                    .centerCrop()
                    .into(holder.picIV);

        holder.defRV.setNestedScrollingEnabled(false);
        holder.defRV.setAdapter(new DefsAdapter(activity, adModel.getDefsList()));

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(activity, DetailsActivity.class);
                intent.putExtra(SHORT_URL, adModel.getShortURl());
                activity.startActivity(intent);
            }
        });
    }


    private int getPicWidth() {
        return (int) ((Functions.getScreenWidth() - (activity.getResources().getDimensionPixelSize(R.dimen.default_spacing_x0_5) * 2)) / 3.2);
    }

    private int getPicHeight() {
        return (int) (getPicWidth() / 1.31);
    }
}