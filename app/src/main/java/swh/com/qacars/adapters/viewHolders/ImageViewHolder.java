package swh.com.qacars.adapters.viewHolders;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import swh.com.qacars.R;

/**
 * ============================================================================
 * 😎😎😎
 * Copyright (c) 12/Jan/2020
 * İstanbul - Türkiye
 * QACars
 * ============================================================================
 **/

public class ImageViewHolder extends RecyclerView.ViewHolder {

    public ImageView picIV;

    public ImageViewHolder(@NonNull View itemView) {
        super(itemView);

        initializing();
    }

    private void initializing() {
        picIV = itemView.findViewById(R.id.row_image_pic_image);
    }
}