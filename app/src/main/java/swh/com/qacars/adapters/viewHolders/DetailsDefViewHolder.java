package swh.com.qacars.adapters.viewHolders;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import swh.com.qacars.R;

/**
 * ============================================================================
 * 😎😎😎
 * Copyright (c) 12/Jan/2020
 * İstanbul - Türkiye
 * QACars
 * ============================================================================
 **/

public class DetailsDefViewHolder extends RecyclerView.ViewHolder {

    public TextView nameTV, valueTV;

    public DetailsDefViewHolder(@NonNull View itemView) {
        super(itemView);

        initializing();
    }

    private void initializing() {
        nameTV = itemView.findViewById(R.id.row_details_def_name_text);
        valueTV = itemView.findViewById(R.id.row_details_def_value_text);
    }
}
