package swh.com.qacars.repository;

import android.util.Log;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.MutableLiveData;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import swh.com.qacars.common.LoadingView;
import swh.com.qacars.helpers.ModelsHelper;
import swh.com.qacars.models.AdModel;
import swh.com.qacars.sync.Connection;

import static swh.com.qacars.common.Constants.*;

public class AdRepository {

    private static AdRepository instance;
    private MutableLiveData<List<AdModel>> adsMutableLiveData;
    private MutableLiveData<AdModel> adDetailsMutableLiveData;

    public static AdRepository getInstance() {
        if (instance == null)
            instance = new AdRepository();
        return instance;
    }

    public void getAdsList(final AppCompatActivity activity, View view, int pageNumber) {
        if (adsMutableLiveData == null)
            adsMutableLiveData = new MutableLiveData<>();
        fetchAdsList(activity, view, pageNumber);
    }

    public void getAdDetails(final AppCompatActivity activity, View view, String shortUrl) {
        //if (adDetailsMutableLiveData == null)
            adDetailsMutableLiveData = new MutableLiveData<>();
        fetchAdsDetails(activity, view, shortUrl);
    }

    public MutableLiveData<List<AdModel>> getAdsMutableLiveData() {
        return adsMutableLiveData;
    }

    public MutableLiveData<AdModel> getAdDetailsMutableLiveData() {
        return adDetailsMutableLiveData;
    }

    private void fetchAdsList(final AppCompatActivity activity, View view, int pageNumber) {
        final LoadingView loadingView = new LoadingView(activity, view);
        loadingView.showLoading(pageNumber > 1);

        try {
            final JSONObject jsonBody = new JSONObject();
            jsonBody.put("CategoryId", "2005");
            jsonBody.put("adsType", "0");
            jsonBody.put("Options", new JSONArray());
            jsonBody.put("CountryCode", "qa");
            jsonBody.put("LanguageId", "en");
            jsonBody.put("PageNumber", pageNumber);
            jsonBody.put("RowsPerPage", 20);
            jsonBody.put("OrderBy", 1);

            Connection.getInstance(activity).getRequestQueue().add(new StringRequest(Request.Method.POST, URL_API_LIST,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {

                                JSONObject mainJSONObject = new JSONObject(response);

                                // check if I call this function from the main list
                                if (mainJSONObject.has(ADS)) {
                                    JSONArray jsonArray = mainJSONObject.getJSONArray(ADS);
                                    ArrayList<AdModel> adsArrayList = new ArrayList<>();
                                    for (int i = 0; i < jsonArray.length(); i++)
                                        adsArrayList.add(ModelsHelper.getAdModel(jsonArray.getJSONObject(i)));

                                    adsMutableLiveData.setValue(adsArrayList);
                                }

                                loadingView.hideLoading();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                        }
                    }) {

                @Override
                public String getBodyContentType() {
                    return "application/json; charset=utf-8";
                }

                @Override
                public byte[] getBody() {
                    return jsonBody.toString().getBytes(StandardCharsets.UTF_8);
                }

                @Override
                public Map<String, String> getHeaders() {
                    HashMap<String, String> header = new HashMap<>();
                    header.put("Accept-Language", "en");
                    return header;
                }
            }).setRetryPolicy(getRetryPolicy());
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void fetchAdsDetails(final AppCompatActivity activity, View view, String shortUrl) {
        final LoadingView loadingView = new LoadingView(activity, view);
        loadingView.showLoading(false);

        Log.e("Url", String.format(URL_API_DETAILS, shortUrl));

        Connection.getInstance(activity).getRequestQueue().add(new StringRequest(String.format(URL_API_DETAILS, shortUrl),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject mainJSONObject = new JSONObject(response);

                            if (mainJSONObject.has(RESULT))
                                adDetailsMutableLiveData.setValue(ModelsHelper.getAdModel(mainJSONObject.getJSONObject(RESULT)));

                            loadingView.hideLoading();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }) {

            @Override
            public Map<String, String> getHeaders() {
                HashMap<String, String> header = new HashMap<>();
                header.put("Accept-Language", "en");

                return header;
            }
        }).setRetryPolicy(getRetryPolicy());
    }

    private RetryPolicy getRetryPolicy() {
        return new DefaultRetryPolicy(26 * 1000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
    }
}