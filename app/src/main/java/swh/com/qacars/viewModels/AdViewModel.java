package swh.com.qacars.viewModels;

import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import java.util.List;

import swh.com.qacars.models.AdModel;
import swh.com.qacars.repository.AdRepository;

/**
 * ============================================================================
 * 😎😎😎
 * Copyright (c) 12/Jan/2020 - SEMİCOLON WEB HİZMETLERİ TİC. LTD. ŞTİ
 * İstanbul - Türkiye
 * www.semicolon.com.tr - merhaba@semicolon.com.tr
 * QACars
 * ============================================================================
 **/

public class AdViewModel extends ViewModel {

    private LiveData<List<AdModel>> adsLiveData;
    private LiveData<AdModel> adDetailsLiveData;
    private AdRepository adRepository;

    public void init() {
        if (adRepository == null)
            adRepository = AdRepository.getInstance();
    }

    public void initList(AppCompatActivity activity, View view, int pageNumber) {
        if (adsLiveData != null)
            return;

        loadList(activity, view, pageNumber);

        adsLiveData = adRepository.getAdsMutableLiveData();
    }

    public void initDetails(AppCompatActivity activity, View view, String shortUrl) {
        if (adDetailsLiveData != null)
            return;

        adRepository.getAdDetails(activity, view, shortUrl);

        adDetailsLiveData = adRepository.getAdDetailsMutableLiveData();
    }

    public void loadList(AppCompatActivity activity, View view, int pageNumber) {
        adRepository.getAdsList(activity, view, pageNumber);
    }

    public LiveData<List<AdModel>> getAds() {
        return adsLiveData;
    }

    public LiveData<AdModel> getAdDetails() {
        return adDetailsLiveData;
    }
}
