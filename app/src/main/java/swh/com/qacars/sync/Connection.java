package swh.com.qacars.sync;

import android.content.Context;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

public class Connection {

    private static Connection instance = null;
    private RequestQueue requestQueue;

    private Connection(Context context) {
        requestQueue = Volley.newRequestQueue(context);
    }

    public static Connection getInstance(Context context) {
        if (instance == null)
            instance = new Connection(context);
        return instance;
    }

    public RequestQueue getRequestQueue() {
        return requestQueue;
    }
}