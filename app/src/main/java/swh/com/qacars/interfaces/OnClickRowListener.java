package swh.com.qacars.interfaces;

/**
 * ============================================================================
 * 😎😎😎
 * Copyright (c) 25/Mar/2018 - SEMİCOLON WEB HİZMETLERİ TİC. LTD. ŞTİ
 * İstanbul - Türkiye
 * Osman ALSIKH - Android developer
 * www.semicolon.com.tr - merhaba@semicolon.com.tr
 * ============================================================================
 **/

public interface OnClickRowListener {
    void onClickRow(Object object);
}