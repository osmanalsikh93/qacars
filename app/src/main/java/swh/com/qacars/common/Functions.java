package swh.com.qacars.common;

import android.content.res.Resources;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.core.content.res.ResourcesCompat;

import java.util.Locale;

import swh.com.qacars.R;

/**
 * ============================================================================
 * 😎😎😎
 * Copyright (c) 30/Nov/2019 - SEMİCOLON WEB HİZMETLERİ TİC. LTD. ŞTİ
 * İstanbul - Türkiye
 * Osman ALSIKH - Android developer
 * www.semicolon.com.tr - merhaba@semicolon.com.tr
 * ============================================================================
 **/

public class Functions {

    public static int getScreenWidth() {
        return Resources.getSystem().getDisplayMetrics().widthPixels;
    }

    public static int dpToPx(int dp) {
        return (int) (dp * Resources.getSystem().getDisplayMetrics().density);
    }

    public static SpannableStringBuilder getPriceText(AppCompatActivity activity, double price, String symbol) {
        SpannableStringBuilder stringBuilder = new SpannableStringBuilder(String.format(Locale.ENGLISH, "%.2f %s", price, symbol));
        stringBuilder.setSpan(new CustomTypefaceSpan(ResourcesCompat.getFont(activity, R.font.nunito_bold)), 0, stringBuilder.length() - symbol.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
        stringBuilder.setSpan(new CustomTypefaceSpan(ResourcesCompat.getFont(activity, R.font.nunito)), stringBuilder.length() - symbol.length(), stringBuilder.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
        stringBuilder.setSpan(new ForegroundColorSpan(ContextCompat.getColor(activity, R.color.primaryColor)),
                0, stringBuilder.length() - symbol.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        return stringBuilder;
    }
}