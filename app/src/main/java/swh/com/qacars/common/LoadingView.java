package swh.com.qacars.common;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.appcompat.app.AppCompatActivity;

import swh.com.qacars.R;

public class LoadingView {

    private AppCompatActivity activity;

    private ViewGroup viewGroup;
    private View view, loadingView;

    public LoadingView(AppCompatActivity activity, View view) {
        this.activity = activity;
        this.view = view;

        viewGroup = (ViewGroup) this.view;
    }

    private LayoutInflater getInflate() {
        return (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    private void setupLoadingView(boolean showLoadingBackground) {
        loadingView = getInflate().inflate(R.layout.view_loading, (ViewGroup) view, false);
        if (showLoadingBackground) // if the view refresh add the background color.
            loadingView.setBackgroundColor(Color.parseColor("#4Dffffff"));
    }

    public void showLoading(boolean showLoadingBackground) {
        if (loadingView == null) {
            setupLoadingView(showLoadingBackground);
            viewGroup.addView(loadingView);
        }
    }

    public void hideLoading() {
        if (loadingView != null)
            viewGroup.removeView(loadingView);
        loadingView = null;
    }
}