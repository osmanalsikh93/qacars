package swh.com.qacars.common;

public class Constants {

    // api
    private static final String URL_BASE = "https://test.yousale.com/api/ad/";
    public static final String URL_API_LIST = URL_BASE + "GetAdsList";
    public static final String URL_API_DETAILS = URL_BASE + "details?id=%s&languageCode=en";

    //
    public static final String ADS = "Ads";
    public static final String RESULT = "Result";

    //
    public static final String TITLE = "Title";
    public static final String SHORT_URL = "ShortUrl";
    public static final String COVER = "Cover";
    public static final String CURRENCY_SYMBOL = "CurrencySymbol";
    public static final String PHONE_CODE = "PhoneCode";
    public static final String PHONE = "PhoneNumber";
    public static final String ADDRESS = "ComplementAddress";
    public static final String IS_FEATURED = "IsFeatured";
    public static final String SELLING_PRICE = "SellingPrice";
    public static final String DEFS = "Defs";
    public static final String AD_DEFS = "AdDefs";
    public static final String THUMBS = "ThumpTmages";
    public static final String IMAGES = "Images";

    public static final String DEF_NAME = "DefName";
    public static final String DEF_VALUE = "DefValue";
    public static final String VALUE = "Value";
}