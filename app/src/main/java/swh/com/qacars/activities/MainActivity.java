package swh.com.qacars.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import swh.com.qacars.R;
import swh.com.qacars.adapters.AdsAdapter;
import swh.com.qacars.common.Constants;
import swh.com.qacars.models.AdModel;
import swh.com.qacars.viewModels.AdViewModel;

public class MainActivity extends AppCompatActivity {

    private int currentPage = 1;
    private boolean isLoading;
    private List<AdModel> adsList;
    private AdViewModel adViewModel;

    private RecyclerView adsRV;

    private AdsAdapter adsAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        init();
        setup();
    }

    private void init() {
        adsList = new ArrayList<>();
        adsRV = findViewById(R.id.activity_main_ads_recycler);

        adViewModel = ViewModelProviders.of(this).get(AdViewModel.class);

        adsAdapter = new AdsAdapter(this, adsList);
    }

    private void setup() {
        adsRV.setAdapter(adsAdapter);
        adsRV.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                // get total item count.
                int totalItemCount = adsRV.getLayoutManager().getItemCount();
                // get last visible item.
                int lastVisibleItem = ((LinearLayoutManager) adsRV.getLayoutManager()).findLastVisibleItemPosition() + 4;

                /*
                  start loading new page if
                  1- isLoading is false. This prevents load next page if the previous page not loaded yet.
                  2- totalItemCount <= (lastVisibleItem + 4) Means that start load the next page before reaching the bottom of the list by 4 rows.
                 */
                if (!isLoading && (totalItemCount <= lastVisibleItem)) {
                    adsRV.post(new Runnable() {
                        @Override
                        public void run() {
                            currentPage++;
                            adViewModel.loadList(MainActivity.this, findViewById(R.id.activity_main_frame_layout), currentPage);
                        }
                    });
                    isLoading = true;
                }
            }
        });

        adViewModel.init();
        adViewModel.initList(this, findViewById(R.id.activity_main_frame_layout), currentPage);
        adViewModel.getAds().observe(this, new Observer<List<AdModel>>() {
            @Override
            public void onChanged(List<AdModel> adModels) {
                if (currentPage == 1)
                    adsList.clear();
                adsList.addAll(adModels);

                if (currentPage == 1)
                    adsAdapter.notifyDataSetChanged();
                else adsAdapter.notifyItemChanged(adsList.size());

                isLoading = false;
            }
        });
    }
}