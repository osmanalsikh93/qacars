package swh.com.qacars.activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager2.widget.ViewPager2;

import com.rd.PageIndicatorView;

import java.util.ArrayList;
import java.util.List;

import swh.com.qacars.R;
import swh.com.qacars.adapters.DetailsDefsAdapter;
import swh.com.qacars.adapters.ImagesAdapter;
import swh.com.qacars.common.Constants;
import swh.com.qacars.common.Functions;
import swh.com.qacars.interfaces.OnClickRowListener;
import swh.com.qacars.models.AdModel;
import swh.com.qacars.models.DefModel;
import swh.com.qacars.viewModels.AdViewModel;

public class DetailsActivity extends AppCompatActivity implements View.OnClickListener, OnClickRowListener {

    private String shortUrl;
    private List<String> imagesList, thumbsList;
    private List<DefModel> defsList;
    private AdViewModel adViewModel;

    private View mainLayout, phoneLayout;
    private TextView titleTV, priceTV, addressTV, phoneTV;
    private ImageView backIV;
    private ViewPager2 imagesVP;
    private RecyclerView thumbsRV, defRV;
    private PageIndicatorView imagesPIV;

    private ImagesAdapter imagesAdapter, thumbsAdapter;
    private DetailsDefsAdapter detailsDefsAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        init();
        setup();
    }

    private void init() {
        shortUrl = getIntent().getStringExtra(Constants.SHORT_URL);
        imagesList = new ArrayList<>();
        thumbsList = new ArrayList<>();
        defsList = new ArrayList<>();

        adViewModel = ViewModelProviders.of(this).get(AdViewModel.class);

        mainLayout = findViewById(R.id.activity_details_main_layout);
        phoneLayout = findViewById(R.id.activity_details_phone_layout);

        titleTV = findViewById(R.id.activity_details_title_text);
        priceTV = findViewById(R.id.activity_details_price_text);
        addressTV = findViewById(R.id.activity_details_location_text);
        phoneTV = findViewById(R.id.activity_details_phone_text);

        backIV = findViewById(R.id.view_toolbar_details_back_image);

        imagesVP = findViewById(R.id.activity_details_images_pager);

        thumbsRV = findViewById(R.id.activity_details_thumbs_recycler);
        defRV = findViewById(R.id.activity_details_defs_recycler);

        imagesPIV = findViewById(R.id.activity_details_images_indicator);

        imagesAdapter = new ImagesAdapter(this, imagesList, false);
        thumbsAdapter = new ImagesAdapter(this, thumbsList, true);
        detailsDefsAdapter = new DetailsDefsAdapter(this, defsList);
    }

    private void setup() {
        mainLayout.setVisibility(View.GONE);

        imagesVP.setAdapter(imagesAdapter);
        imagesVP.getLayoutParams().height = (int) (Functions.getScreenWidth() / 1.5);
        imagesVP.registerOnPageChangeCallback(new ViewPager2.OnPageChangeCallback() {
            @Override
            public void onPageSelected(int position) {
                super.onPageSelected(position);
                imagesPIV.setSelection(position);
            }
        });

        thumbsRV.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));
        thumbsRV.setAdapter(thumbsAdapter);

        defRV.setNestedScrollingEnabled(false);
        defRV.setAdapter(detailsDefsAdapter);

        adViewModel.init();
        adViewModel.initDetails(this, findViewById(R.id.activity_details_frame_layout), shortUrl);
        adViewModel.getAdDetails().observe(this, new Observer<AdModel>() {
            @Override
            public void onChanged(AdModel adModel) {
                setData(adModel);
            }
        });

        backIV.setOnClickListener(this);
        phoneTV.setOnClickListener(this);
    }

    private void setData(AdModel adModel) {
        titleTV.setText(adModel.getTitle());
        priceTV.setText(Functions.getPriceText(this, adModel.getPrice(), adModel.getCurrencySymbol()));
        addressTV.setText(adModel.getAddress());
        phoneTV.setText(String.format("%s%s", adModel.getPhoneCode(), adModel.getPhone()));

        defsList.clear();
        defsList.addAll(adModel.getDefsList());
        detailsDefsAdapter.notifyDataSetChanged();

        if (adModel.getImagesList() != null && adModel.getImagesList().size() > 0) {
            imagesList.clear();
            imagesList.addAll(adModel.getImagesList());
            imagesAdapter.notifyDataSetChanged();

            imagesPIV.setCount(imagesList.size());
        }

        if (adModel.getThumbsList() != null && adModel.getThumbsList().size() > 0) {
            thumbsList.clear();
            thumbsList.addAll(adModel.getThumbsList());
            thumbsAdapter.notifyDataSetChanged();
        }

        // set visibility for views
        mainLayout.setVisibility(View.VISIBLE);
        if (adModel.getAddress().contentEquals("null") || adModel.getAddress().isEmpty())
            addressTV.setVisibility(View.GONE);
        if (adModel.getPhone() == null || adModel.getPhone().isEmpty())
            phoneLayout.setVisibility(View.GONE);
    }

    @Override
    public void onClickRow(Object object) {
        int selectedPosition = (int) object;

        imagesVP.setCurrentItem(selectedPosition);
        imagesPIV.setSelection(selectedPosition);
    }

    @Override
    public void onClick(View view) {
        if (view == backIV) {
            finish();
        } else if (view == phoneTV) {
            Intent intent = new Intent(Intent.ACTION_DIAL);
            intent.setData(Uri.parse(String.format("tel:%s", phoneTV.getText().toString())));
            startActivity(intent);
        }
    }
}