package swh.com.qacars.helpers;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import swh.com.qacars.models.AdModel;
import swh.com.qacars.models.DefModel;

import static swh.com.qacars.common.Constants.*;

public class ModelsHelper {

    public static AdModel getAdModel(JSONObject jsonObject) {
        AdModel adModel = new AdModel();
        try {
            if (jsonObject.has(TITLE)) {
                adModel.setTitle(jsonObject.getString(TITLE));
            }
            if (jsonObject.has(SHORT_URL)) {
                adModel.setShortURl(jsonObject.getString(SHORT_URL));
            }
            if (jsonObject.has(COVER)) {
                adModel.setImageUrl(jsonObject.getString(COVER));
            }
            if (jsonObject.has(CURRENCY_SYMBOL)) {
                adModel.setCurrencySymbol(jsonObject.getString(CURRENCY_SYMBOL));
            }
            if (jsonObject.has(PHONE_CODE)) {
                adModel.setPhoneCode(jsonObject.getString(PHONE_CODE));
            }
            if (jsonObject.has(PHONE)) {
                adModel.setPhone(jsonObject.getString(PHONE));
            }
            if (jsonObject.has(ADDRESS)) {
                adModel.setAddress(jsonObject.getString(ADDRESS));
            }
            if (jsonObject.has(IS_FEATURED)) {
                adModel.setFeatured(jsonObject.getBoolean(IS_FEATURED));
            }
            if (jsonObject.has(SELLING_PRICE)) {
                adModel.setPrice(jsonObject.getDouble(SELLING_PRICE));
            }
            if (jsonObject.has(DEFS) || jsonObject.has(AD_DEFS)) {
                JSONArray defsJSONArray = jsonObject.getJSONArray(jsonObject.has(DEFS) ? DEFS : AD_DEFS);
                List<DefModel> defsList = new ArrayList<>();
                for (int i = 0; i < defsJSONArray.length(); i++)
                    defsList.add(getDefModel(defsJSONArray.getJSONObject(i)));
                adModel.setDefsList(defsList);
            }
            if (jsonObject.has(IMAGES)) {
                JSONArray imagesJSONArray = jsonObject.getJSONArray(IMAGES);
                List<String> imagesList = new ArrayList<>();
                for (int i = 0; i < imagesJSONArray.length(); i++)
                    imagesList.add(imagesJSONArray.getString(i));
                adModel.setImagesList(imagesList);
            }
            if (jsonObject.has(THUMBS)) {
                JSONArray thumbsJSONArray = jsonObject.getJSONArray(THUMBS);
                List<String> thumbsList = new ArrayList<>();
                for (int i = 0; i < thumbsJSONArray.length(); i++)
                    thumbsList.add(thumbsJSONArray.getString(i));
                adModel.setThumbsList(thumbsList);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return adModel;
    }

    public static DefModel getDefModel(JSONObject jsonObject) {
        DefModel defModel = new DefModel();
        try {
            if (jsonObject.has(DEF_NAME))
                defModel.setName(jsonObject.getString(DEF_NAME));
            if (jsonObject.has(DEF_VALUE) || jsonObject.has(VALUE))
                defModel.setValue(jsonObject.getString(jsonObject.has(DEF_VALUE) ? DEF_VALUE : VALUE));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return defModel;
    }
}
