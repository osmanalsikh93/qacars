package swh.com.qacars.models;

/**
 * ============================================================================
 * 😎😎😎
 * Copyright (c) 12/Jan/2020 - SEMİCOLON WEB HİZMETLERİ TİC. LTD. ŞTİ
 * İstanbul - Türkiye
 * www.semicolon.com.tr - merhaba@semicolon.com.tr
 * QACars
 * ============================================================================
 **/

public class DefModel {

    private String name, value;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
