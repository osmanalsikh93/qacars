package swh.com.qacars.models;

import java.util.List;

/**
 * ============================================================================
 * 😎😎😎
 * Copyright (c) 12/Jan/2020
 * İstanbul - Türkiye
 * QACars
 * ============================================================================
 **/

public class AdModel {

    private String title, shortURl, imageUrl, currencySymbol, address, phoneCode, phone;
    private boolean isFeatured;
    private double price;
    private List<String> imagesList, thumbsList;
    private List<DefModel> defsList;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getShortURl() {
        return shortURl;
    }

    public void setShortURl(String shortURl) {
        this.shortURl = shortURl;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getCurrencySymbol() {
        return currencySymbol;
    }

    public void setCurrencySymbol(String currencySymbol) {
        this.currencySymbol = currencySymbol;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhoneCode() {
        return phoneCode;
    }

    public void setPhoneCode(String phoneCode) {
        this.phoneCode = phoneCode;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public boolean isFeatured() {
        return isFeatured;
    }

    public void setFeatured(boolean featured) {
        isFeatured = featured;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public List<String> getImagesList() {
        return imagesList;
    }

    public void setImagesList(List<String> imagesList) {
        this.imagesList = imagesList;
    }

    public List<String> getThumbsList() {
        return thumbsList;
    }

    public void setThumbsList(List<String> thumbsList) {
        this.thumbsList = thumbsList;
    }

    public List<DefModel> getDefsList() {
        return defsList;
    }

    public void setDefsList(List<DefModel> defsList) {
        this.defsList = defsList;
    }
}
